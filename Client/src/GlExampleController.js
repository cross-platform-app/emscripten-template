/**
 * Controller to handle the logic that is needed for the OpenGL-example
 */
class GlExampleController {
    constructor() {
        // fetching canvas from html
        this._canvas = document.getElementById("canvasGlExample");
        // expose canvas to the Emscripten-module
        moduleGlExample.canvas = this._canvas;

        // initialize glExample
        this._glExample = moduleGlExample.GlExampleInterface.GlExample();
        this._glExample.setColor(.3,.2,.8);

        this._canvasWidth = this._canvas.width;
        this._canvasHeight = this._canvas.height;
        // optimize canvas for high-resolution displays
        this.resizeForHighResolutions();
        // initialize counter for resize-checking. This will be increased every frame to check every
        // 200 Frames if the resolution of the screen has changed. This is needed to support moving the window from
        // a low-res display over to a high-res one. The resolution change will be detected and the canvas-size will be
        // corrected
        this._resizeCheckCounter = 0;
        // start rendering loop
        let thiz =this;
        window.requestAnimationFrame(function() {
            thiz.render();
        });

    }

    /**
     * Enables hi-res support for the canvas element. Scales the canvas up so that canvas-pixels match the real number
     * of pixels required to draw the image in full resolution. The up-scaled image is then scaled down by CSS again, so
     * that it matches its original size.
     */
    resizeForHighResolutions() {
        // configure canvas for hires-display-support
        let devicePixelRatio = window.devicePixelRatio || 1;

        this._canvas.width = this._canvasWidth * devicePixelRatio;
        this._canvas.height = this._canvasHeight * devicePixelRatio;
        this._canvas.style.width = this._canvasWidth + "px";
        this._canvas.style.height = this._canvasHeight + "px";
    }

    /**
     * rendering the actual image. Checks every 200 seconds if the resolution is still the same, to correct the image if
     * it changed.
     * Then sets the viewport size and calls the native code to render the image
     */
    render() {
        if(this._resizeCheckCounter++ > 200) {
            this._resizeCheckCounter = 0;
            this.resizeForHighResolutions();
        }
        this._glExample.setViewport(this._canvas.width, this._canvas.height);
        this._glExample.render();
        let thiz = this;
        window.requestAnimationFrame(function() {
            thiz.render();
        });
    }

}