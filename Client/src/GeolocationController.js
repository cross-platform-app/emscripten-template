/**
 * Controller that manages the logic to display the geolocation-example
 */
class GeolocationController {
    constructor() {
        // fetching elements from html
        this._buttonUpdateLocation = document.getElementById("buttonUpdateLocation");
        this._labelLatitude = document.getElementById("labelLatitude");
        this._labelLongitude = document.getElementById("labelLongitude");
        this._spinnerUpdateLocation = document.getElementById("spinnerUpdateLocation");
        this._labelLocationError = document.getElementById("labelLocationError");

        // enable material design Ripple effect on the button
        mdc.ripple.MDCRipple.attachTo(this._buttonUpdateLocation);

        // initialize LocationService-object
        this._locationService = moduleCore.LocationServiceInterface.LocationService(castToSmartPtr(new EmscriptenLocation()));

        // derive OnChangeListener for location updates
        let OnLocationChangeListener = moduleCore.OnChangeListener.extend("OnChangeListener", {
            __construct: function(controller) {
                this.__parent.__construct.call(this);
                this._controller = controller;
            },
            onSuccess: function () {
                this._controller.updateLocationDisplay();
                this._controller.setStateLocationLoading(false);
            },
            onError: function () {
                this._controller.setLocationErrorVisible();
                this._controller.setStateLocationLoading(false);
            }
        });
        // instantiate OnChangeListener and register on LocationService-object
        let onLocationChangeListener = new OnLocationChangeListener(this);
        this._locationService.onLocationChanged(castToSmartPtr(onLocationChangeListener));

        //connect button signals
        let thiz = this;
        this._buttonUpdateLocation.addEventListener("click", function(event) {
            thiz.onButtonUpdateLocationClicked();
        });
    }

    /**
     * updates the `span` elements that display the current latitude and longitude
     */
    updateLocationDisplay() {
        let location = this._locationService.getLocation();
        this._labelLatitude.innerHTML = location.lat;
        this._labelLongitude.innerHTML = location.lon;
        location.delete();
    }

    /**
     * gets triggered when the user clicks the "Update Location"-button.
     * Locks the button and calls the native function to get the new location.
     */
    onButtonUpdateLocationClicked() {
        this.setStateLocationLoading(true);
        this._locationService.updateLocation();

    }

    /**
     * displays an error in the snackbar. Should be used when fetching the location-update failed for some reason.
     */
    setLocationErrorVisible() {
        let thiz = this;
        snackbar.show({
            message: "Error updating Location!",
            actionText: "Try again",
            actionHandler: function() {
                thiz.onButtonUpdateLocationClicked();
            }
        });
    }

    /**
     * sets the state of the UI
     * @param loading The UI can have two different states:
     *                  - `true`:  the location update is currently running. The user needs to wait for it to finish.
     *                             The "Update Location"-button is disabled and a loading-indicator is visible.
     *                  - `false`: The user can request a location update. No loading-indicator is visible.
     */
    setStateLocationLoading(loading) {
        if(loading) {
            this._buttonUpdateLocation.disabled = true;
            this._spinnerUpdateLocation.style.display = "block";
        } else {
            this._buttonUpdateLocation.disabled = false;
            this._spinnerUpdateLocation.style.display = "none";
        }
    }

}
