/**
 * Javascript-/Browser-specific implementation of the **LocationInterface** from the Logic-Tier.
 * Calls the Browser-API for fetching the device-location and forwards it to the LocationInterface provided by the
 * Logic-Tier
 */
let EmscriptenLocation = moduleCore.LocationInterface.extend("LocationInterface", {
    __construct: function() {
        this.__parent.__construct.call(this);
        this._lat = 0;
        this._lon = 0;
    },
    updateLocation(onChangeListener) {
        // !! cloning the onChangeListener is important, because otherwise it would get destroyed.
        onChangeListener = onChangeListener.clone();
        // check if the geolocation-API is available in this Browser
        if(navigator.geolocation) {
            let thiz = this;
            // asynchronously fetching the location
            navigator.geolocation.getCurrentPosition(
                //success callback
                function(position) {
                    thiz._lat = position.coords.latitude;
                    thiz._lon = position.coords.longitude;
                    onChangeListener.onSuccess();
                },
                // error callback
                function() {
                    onChangeListener.onError();
                }
            )
        } else {
            onChangeListener.onError();
        }
    },
    getLocation() {
        return new moduleCore.LocationRecord(this._lat, this._lon);
    }
});