/**
 * Controller that manages the logic to display the counter-example.
 */
class CounterController {
    constructor() {
        // fetching elements from html
        this._labelCounterValue = document.getElementById("labelCounterValue");
        this._buttonResetCounter =  document.getElementById("buttonResetCounter");
        this._buttonIncreaseCounter = document.getElementById("buttonIncreaseCounter");
        this._spinnerCounterLoading = document.getElementById("spinnerCounterLoading");

        // enable material design ripple effect on the buttons
        mdc.ripple.MDCRipple.attachTo(this._buttonIncreaseCounter);
        mdc.ripple.MDCRipple.attachTo(this._buttonResetCounter);


        // initialize Counter-object
        this._counter = moduleCore.CounterInterface.Counter();

        // derive OnChangeListener for counter change-events
        let OnNumberChangeListener = moduleCore.OnChangeListener.extend("OnChangeListener", {
            __construct: function(controller) {
                this.__parent.__construct.call(this);
                this._controller = controller;
            },
            onSuccess: function () {
                this._controller.setCounterDisplay();
                this._controller.setStateCounterCanInteract(true);
            },
            onError: function () {
                this._controller.setCounterErrorVisible();
                this._controller.setStateCounterCanInteract(true);
            }
        });
        // instantiate onChangeListener and register on counter-object
        let onNumberChangeListener = new OnNumberChangeListener(this);
        this._counter.onNumberChanged(castToSmartPtr(onNumberChangeListener));

        // connect button signals
        let thiz = this;
        this._buttonIncreaseCounter.addEventListener("click", function(event) {
            thiz.onButtonIncreaseCounterClicked();
        });
        this._buttonResetCounter.addEventListener("click", function(event) {
            thiz.onButtonResetCounterClicked();
        });
    }

    /**
     * shows an error message in the snackbar. Should be called if increasing the counter failed
     */
    setCounterErrorVisible() {
        let thiz = this;
        snackbar.show({
            message: "Error increasing Counter!",
            actionText: "Reset",
            actionHandler: function() {
                thiz.onButtonResetCounterClicked();
            }
        });
    };

    /**
     * gets triggered when the user clicks the "Increase Counter"-Button.
     * Locks the UI and calls native code to increase the counter.
     */
    onButtonIncreaseCounterClicked() {
        this.setStateCounterCanInteract(false);
        this._counter.increaseNumber();
    };

    /**
     * gets triggered when the user clicks the "Reset"-Button.
     * Locks the UI and calls native code to reset the counter.
     */
    onButtonResetCounterClicked() {
        this.setStateCounterCanInteract(false);
        this._counter.resetNumber()
    };

    /**
     * updates the `div` that displays the current counter-value with the value stored in the counter-object
     */
    setCounterDisplay() {
        this._labelCounterValue.innerHTML = this._counter.getNumber();
    };

    /**
     * Locks/Unlocks the buttons to increase/reset the counter.
     * @param canInteract The UI can have two states:
     *                      - `true`:  The user can interact with the buttons
     *                      - `false`: The user can't interact with the buttons, because the UI still waits for Feedback
     *                                 from the last interaction. A loading-indicator visible.
     */
    setStateCounterCanInteract(canInteract) {
        if(canInteract) {
            this._buttonIncreaseCounter.disabled = false;
            this._buttonResetCounter.disabled = false;
            this._spinnerCounterLoading.style.display = "none";
        } else {
            this._buttonIncreaseCounter.disabled = true;
            this._buttonResetCounter.disabled = true;
            this._spinnerCounterLoading.style.display = "block";
        }
    };
}
