/**
 * This script initializes all the application logic when the page has loaded.
 */

let counterController;
let geolocationController;
let glExampleController;
let graphicsController;
let snackbar;

window.onload = function () {
    snackbar = new mdc.snackbar.MDCSnackbar(document.getElementById("snackbar"));
    counterController = new CounterController();
    geolocationController = new GeolocationController();
    glExampleController = new GlExampleController();
    graphicsController = new GraphicsController();
};