# Cross-platform-app: Web Client

This demo shows how to use a native library for a web-application by cross-compiling it with _Emscripten_.

The template therefore provides 3 different demos:

1. The basic usage of the [C++-library](https://gitlab.com/cross-platform-app/core-template) from Javascript.
2. The rendering of a [C++-based OpenGL (ES) implementation](https://gitlab.com/cross-platform-app/gl-example).
3. An example how to access a System-API, like the device-location via the [C++-library](https://gitlab.com/cross-platform-app/core-template).

_This demo currently only was successfully built under macOS. On GNU/Linux there seems to be some issue with Hunter._

## Build Dependencies
- emcc >= 1.37.36 (Get it [here](https://github.com/juj/emsdk))
- CMake >= 3.7.1

**Important:** Make sure to also meet the dependencies in the submodules `Core` and `gl-example`

## Build Instructions

1. Initialize all submodules with `git submodule update --init --recursive`

2. Generate Makefile for the cross-compilation with CMake
    ```
    cmake -DCMAKE_TOOLCHAIN_FILE=Core/cmake/Emscripten.cmake -G "Unix Makefiles" -DCMAKE_C_ABI_COMPILED=ON -DCMAKE_CXX_ABI_COMPILED=ON -B_builds -H.
    ```

3. Go to `_builds` and compile the project with
    ```
    make
    ```

4. Open `index.html` in the `_builds`-directory in the Browser.

## Screenshots

<img src="screenshot.png" alt="Screenshot of the demo" width="400"/>


